package tk.labyrinth.gwd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@SpringBootApplication
public class GwdApplication {

	public static void main(String... args) {
		SpringApplication.run(GwdApplication.class, args);
	}
}
