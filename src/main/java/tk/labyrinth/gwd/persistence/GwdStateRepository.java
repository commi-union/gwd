package tk.labyrinth.gwd.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GwdStateRepository extends MongoRepository<GwdState, String> {
	// empty
}
