package tk.labyrinth.gwd.persistence;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.springframework.data.annotation.Id;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GwdState {

	Instant since;

	@Id
	String uid;
}
