package tk.labyrinth.gwd;

import com.flowingcode.vaadin.addons.simpletimer.SimpleTimer;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.Cookie;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import tk.labyrinth.gwd.persistence.GwdState;
import tk.labyrinth.gwd.persistence.GwdStateRepository;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Route("")
public class GwdRootPage extends VerticalLayout {

	private final GwdStateRepository gwdStateRepository;

	@Value("${gwd.proof}")
	private String proof;

	@PostConstruct
	private void postConstruct() {
		{
			setAlignItems(Alignment.CENTER);
			setHeightFull();
		}
		SimpleTimer timer = createTimer(gwdStateRepository.findById("the_one")
				.map(GwdState::getSince)
				.orElse(Instant.now()));
		{
			add(new H1("Time Since Last Leg Joke:"));
			//
			add(timer);
			//
			{
				Button resetButton = new Button("Reset");
				//
				resetButton.addClickListener(event -> {
					Runnable resetRunnable = () -> {
						Instant nextSince = Instant.now();
						//
						gwdStateRepository.save(GwdState.builder()
								.since(nextSince)
								.uid("the_one")
								.build());
						//
						{
							getComponentAt(1).removeFromParent();
							//
							addComponentAtIndex(1, createTimer(nextSince));
						}
					};
					//
					String cookieProof = Stream.of(VaadinService.getCurrentRequest().getCookies())
							.filter(cookie -> Objects.equals(cookie.getName(), "gwd_proof"))
							.map(Cookie::getValue)
							.findFirst()
							.orElse(null);
					//
					if (Objects.equals(cookieProof, proof)) {
						resetRunnable.run();
					} else {
						VerticalLayout dialogLayout = new VerticalLayout();
						Dialog dialog = new Dialog(dialogLayout);
						{
							dialogLayout.setAlignItems(Alignment.CENTER);
						}
						{
							dialogLayout.add("Are you Diana?");
							//
							{
								Button yesButton = new Button("Yes");
								Button noButton = new Button("No");
								//
								HorizontalLayout optionsLayout = new HorizontalLayout();
								{
									yesButton.addClickListener(yesEvent -> renderYes(
											dialogLayout,
											Pair.of(yesButton, noButton),
											() -> {
												resetRunnable.run();
												//
												dialog.close();
											}));
									//
									optionsLayout.add(yesButton);
								}
								{
									noButton.addClickListener(noEvent -> renderNo(
											dialogLayout,
											Pair.of(yesButton, noButton),
											dialog::close));
									//
									optionsLayout.add(noButton);
								}
								dialogLayout.add(optionsLayout);
							}
						}
						dialog.open();
					}
				});
				//
				add(resetButton);
			}
			{
				Div stretch = new Div();
				//
				setFlexGrow(1, stretch);
				//
				add(stretch);
			}
			//
			add(new H2("Get Well Diana™"));
			//
			{
				HorizontalLayout footer = new HorizontalLayout();
				//
				footer.add(new Span("Powered by:"));
				footer.add(new Anchor("https://vaadin.com/", "Vaadin"));
				footer.add(new Anchor("https://render.com/", "Render"));
				//
				add(footer);
			}
			//
			add(new Anchor("https://gitlab.com/commi-union/gwd", "Source"));
		}
	}

	private void renderYes(VerticalLayout dialogLayout, Pair<Button, Button> yesNoButtons, Runnable provedCallback) {
		dialogLayout.getChildren().skip(2).forEach(Component::removeFromParent);
		yesNoButtons.getLeft().addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		yesNoButtons.getRight().removeThemeVariants(ButtonVariant.LUMO_PRIMARY);
		//
		dialogLayout.add(new Span("Prove it:"));
		//
		PasswordField proofField = new PasswordField("Proof");
		{
			dialogLayout.add(proofField);
		}
		{
			Button doItButton = new Button("Reset!");
			//
			doItButton.addClickListener(event -> {
				String enteredProof = proofField.getValue();
				//
				if (Objects.equals(enteredProof, proof)) {
					VerticalLayout rememberDialogLayout = new VerticalLayout();
					Dialog rememberDialog = new Dialog(rememberDialogLayout);
					//
					{
						Consumer<Boolean> rememberCallback = remember -> {
							if (remember) {
								VaadinService.getCurrentResponse().addCookie(new Cookie("gwd_proof", enteredProof));
							}
							//
							provedCallback.run();
							//
							rememberDialog.close();
						};
						//
						rememberDialogLayout.add(new Span("Kom ihåg bevis?"));
						//
						{
							HorizontalLayout optionsLayout = new HorizontalLayout();
							//
							{
								Button jaButton = new Button("Ja");
								//
								jaButton.addClickListener(jaEvent -> rememberCallback.accept(true));
								//
								optionsLayout.add(jaButton);
							}
							{
								Button nejButton = new Button("Nej");
								//
								nejButton.addClickListener(jaEvent -> rememberCallback.accept(false));
								//
								optionsLayout.add(nejButton);
							}
							//
							rememberDialogLayout.add(optionsLayout);
						}
					}
					//
					rememberDialog.open();
				} else {
					dialogLayout.getChildren().skip(5).forEach(Component::removeFromParent);
					//
					dialogLayout.add(new Span("You don't look like one"));
				}
			});
			//
			dialogLayout.add(doItButton);
		}
	}

	public static SimpleTimer createTimer(Instant from) {
		SimpleTimer timer = new SimpleTimer();
		//
		timer.getElement().setProperty("startTime", Integer.MAX_VALUE);
		//
		timer.setCountUp(true);
		timer.setHours(true);
		//
		{
			SimpleTimer triggerTimer = new SimpleTimer();
			//
			triggerTimer.getElement().setProperty("startTime", 0.001);
			triggerTimer.setVisible(false);
			//
			triggerTimer.addTimerEndEvent(event ->
					timer.getElement().setProperty("currentTime", Duration.between(from, Instant.now()).toSeconds()));
			//
			triggerTimer.start();
			//
			timer.getElement().appendChild(triggerTimer.getElement());
		}
		//
		timer.start();
		//
		return timer;
	}

	public static void renderNo(VerticalLayout dialogLayout, Pair<Button, Button> yesNoButtons, Runnable okayCallback) {
		dialogLayout.getChildren().skip(2).forEach(Component::removeFromParent);
		yesNoButtons.getLeft().removeThemeVariants(ButtonVariant.LUMO_PRIMARY);
		yesNoButtons.getRight().addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		//
		dialogLayout.add(new Span("Then don't touch this"));
		//
		{
			Button okay = new Button("Okay...");
			//
			okay.addClickListener(event -> okayCallback.run());
			//
			dialogLayout.add(okay);
		}
	}
}
