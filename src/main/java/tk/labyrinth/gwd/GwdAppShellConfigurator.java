package tk.labyrinth.gwd;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Theme(variant = Lumo.DARK)
public class GwdAppShellConfigurator implements AppShellConfigurator {
	// empty
}
